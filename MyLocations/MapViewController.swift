//
//  MapViewController.swift
//  MyLocations
//
//  Created by Simon McNeil on 2019-07-19.
//  Copyright © 2019 SimonMcNeil. All rights reserved.
//

import UIKit
import MapKit
import CoreData

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!

    var managedObjectContext: NSManagedObjectContext! {
        didSet {
            NotificationCenter.default.addObserver(forName: Notification.Name.NSManagedObjectContextObjectsDidChange, object: managedObjectContext, queue: OperationQueue.main) { _ in
                if self.isViewLoaded {
                    self.updateLocations()
                }
            }
        }
    }
    
    var locations = [Location]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateLocations() //Fetches the locaiton object and displays them on the map when the view loads.
        
        if !locations.isEmpty {
            showLocations()
        }

    }
    
    //MARK:- Actions
    @IBAction func showUser() {
        let region = MKCoordinateRegion(center: mapView.userLocation.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapView.setRegion(region, animated: true)
    }
    
    @IBAction func showLocations() {
        let theRegion = region(for: locations)
        mapView.setRegion(theRegion, animated: true)
    }
    
    //MARK:- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditLocation" {
            let controller = segue.destination as! LocationDetailsViewController
            controller.managedObjectContext = managedObjectContext
            
            
            /* Here we get the Location object to edit from the locations array, using the tag property of the sender button as the index in that array */
            let button = sender as! UIButton
            print(button.tag)
            let location = locations[button.tag]
            controller.locationToEdit = location
        }
    }
    
    func region(for annotations: [MKAnnotation]) -> MKCoordinateRegion {
        let region: MKCoordinateRegion
        
        switch annotations.count {
        case 0:
            region = MKCoordinateRegion(center: mapView.userLocation.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
            
        case 1:
            let annotation = annotations[annotations.count - 1]
            region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
            
        default:
            var topLeft = CLLocationCoordinate2D(latitude: -90, longitude: 280)
            var bottomRight = CLLocationCoordinate2D(latitude: 90, longitude: -280)
            
            //Get position of annotations and make calculations to try and be able to view all of them on your map.
            for annotation in annotations {
                topLeft.latitude = max(topLeft.latitude, annotation.coordinate.latitude)
                topLeft.longitude = min(topLeft.longitude, annotation.coordinate.longitude)
                
                bottomRight.latitude = min(bottomRight.latitude, annotation.coordinate.latitude)
                bottomRight.longitude = max(bottomRight.longitude, annotation.coordinate.longitude)
            }
            
            let center = CLLocationCoordinate2D(latitude: topLeft.latitude - (topLeft.latitude - bottomRight.latitude) / 2, longitude: topLeft.longitude - (topLeft.longitude - bottomRight.longitude) / 2)
            
            let extraSpace = 1.1
            let span = MKCoordinateSpan(latitudeDelta: abs(topLeft.latitude - bottomRight.latitude) * extraSpace, longitudeDelta: abs(topLeft.longitude - bottomRight.longitude) * extraSpace)
            
            region = MKCoordinateRegion(center: center, span: span)
        }
        
        return mapView.regionThatFits(region)
        
    }
    
    //MARK:- Helper Methods
    func updateLocations() {
        mapView.removeAnnotations(locations)
        
        let entity = Location.entity()
        
        let fetchRequest = NSFetchRequest<Location>()
        fetchRequest.entity = entity
        
        locations = try! managedObjectContext.fetch(fetchRequest)
        mapView.addAnnotations(locations) //adds a pin for each location on the map
    }

}

extension MapViewController: MKMapViewDelegate {
    
    //creating custom annotations
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        mapView.tintColor = UIColor.red //changes the dot
        
        guard annotation is Location else {
            return nil
        }
        
        let identifier = "Location"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        if annotationView == nil {
            let pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            
            pinView.isEnabled = true
            pinView.canShowCallout = true //boolean value to determine if can show extra information in bubble.
            pinView.animatesDrop = true //boolean value to determine pin be animated to the screen
            pinView.pinTintColor = UIColor(displayP3Red: 0.32, green: 0.8, blue: 0.4, alpha: 1) //custom color for the pin drop
            pinView.tintColor = UIColor(white: 0.0, alpha: 0.5) //custom color for the info on the call out when tapping on the pin drop
            
            let rightButton = UIButton(type: .detailDisclosure)
            rightButton.addTarget(self, action: #selector(showLocationDetails(_:)), for: .touchUpInside)
            pinView.rightCalloutAccessoryView = rightButton //will display the view beside the button
            
            annotationView = pinView
        }
        
        if let annotationView = annotationView {
            annotationView.annotation = annotation
            
            let button = annotationView.rightCalloutAccessoryView as! UIButton
            
            if let index = locations.firstIndex(of: annotation as! Location ) {
                button.tag = index
            }
        }
        return annotationView
    }
    
    @objc func showLocationDetails(_ sender: UIButton) {
        //we use the sender UIButton in the override prepare func
        performSegue(withIdentifier: "EditLocation", sender: sender)
    }
}
