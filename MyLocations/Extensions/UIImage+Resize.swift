//
//  UIImage+Resize.swift
//  MyLocations
//
//  Created by Simon McNeil on 2019-08-03.
//  Copyright © 2019 SimonMcNeil. All rights reserved.
//

import UIKit

//Extension to scale down the images before letting them be displayed in the cell.
extension UIImage {
    func resized(withBounds bounds: CGSize) -> UIImage {
        let horizontalRatio = bounds.width / size.width
        let verticalRatio = bounds.height / bounds.width
        let ratio = min(horizontalRatio, verticalRatio) //min () returns the lesser of the two values
        let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
        
        UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
        
        draw(in: CGRect(origin: CGPoint.zero, size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
