//
//  String+AddText.swift
//  MyLocations
//
//  Created by Simon McNeil on 2019-08-03.
//  Copyright © 2019 SimonMcNeil. All rights reserved.
//

import UIKit

//Useful extension to concatenate strings
extension String {
    mutating func add(text: String?, separatedBy: String = "") {
        if let text = text  {
            if !isEmpty {
                self += separatedBy
            }
            self += text
        }
    }
}
