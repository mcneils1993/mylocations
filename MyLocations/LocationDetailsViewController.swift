//
//  LocationDetailsViewController.swift
//  MyLocations
//
//  Created by Simon McNeil on 2019-06-27.
//  Copyright © 2019 SimonMcNeil. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

/* The reason why this is here, is because we create DateFormatter just once, and then re-use that same object over and over. This is due to DateFormatter
    being relatively expensive to create, as it takes a while to initialize, and may slow down your app and drain its battery quicker.
 
    The trick is that we don't want to create the DateFormatter object until it is needed. This principle is called lazy loading.
 
    Note that we created a private global constant, this is a constant that lives outside of the class but is only visible to this class. */
private let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .short
    return formatter
}()


class LocationDetailsViewController: UITableViewController {

    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var addPhotoLabel: UILabel!
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    
    var coordinate = CLLocationCoordinate2DMake(0, 0) //contains the latitue and longitude from the CLLocation object that we received from the location manager
    var placemark: CLPlacemark? //used to get are address that we've obtained through geocoding
    
    var categoryName = "No Category"
    
    var managedObjectContext: NSManagedObjectContext!

    var date = Date() //need to store the current date in the new Location object.
    
    var descriptionText = ""
    
    var observer: Any! //to deinit are background notification.
    
    //var image: UIImage? //instance variable to hold the picked image
    var image: UIImage? {
        didSet {
            if let image = image {
                imageView.image = image
                imageView.isHidden = false
                addPhotoLabel.text = ""
                imageHeight.constant = 260
                tableView.reloadData()
            }
        }
    }
    
    /* If a variable has a didSet block, then the code in this block is performed whenever you put a new value into that variable
     Here, we take the opportunity to fill in the view controller's instance variables with the Location's object values.
     
     Because prepare(for: sender) - and therefore locationToEdit didSet is called before viewDidLoad(), this puts the right values
     on the screen before it becomes visible
     */
    var locationToEdit: Location? {
        didSet {
            if let location = locationToEdit {
                descriptionText = location.locationDescription
                categoryName = location.category
                date = location.date
                coordinate = CLLocationCoordinate2DMake(location.latitude, location.longitude)
                placemark = location.placemark
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let locationToEdit = locationToEdit {
            title = "Edit Location"
            if locationToEdit.hasPhoto {
                if let imageToShow = locationToEdit.photoImage {
                    image = imageToShow
                }
            }
        }
        
        descriptionTextView.text = descriptionText
        categoryLabel.text = categoryName
        
        latitudeLabel.text = String(format: "%.8f", coordinate.latitude)
        longitudeLabel.text = String(format: "%.8f", coordinate.longitude)
        
        if let placemark = placemark {
            addressLabel.text = string(from: placemark)
        } else {
            addressLabel.text = "No Address Found"
        }
        
        dateLabel.text = format(date: date)
        
        //Hiding the keyboard by tapping anywhere on the screen
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        gestureRecognizer.cancelsTouchesInView = false
        tableView.addGestureRecognizer(gestureRecognizer)
        
        listenForBackgroundNotification()
    }
    
    deinit {
        print("*** deinit \(self)")
        NotificationCenter.default.removeObserver(observer)
    }
    
    //MARK:- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PickCategory" {
            let controller = segue.destination as! CategoryPickerViewController
            controller.selectedCategoryName = categoryName
        }
    }
    
    //MARK:- Actions
    @IBAction func done() {
        //navigationController?.popViewController(animated: true)
        
        /*  We send the naviagtionController view instead of LocationDetailsViewController view because if we didn't then the HUD
            and the isUserInteractionEnabled property wouldn't be applicable for the done and cancel buttons. */
        let hudView = HudView.hud(inView: navigationController!.view, animated: true)
        
        let location: Location
        if let temp = locationToEdit {
            //Text will say updated when the user is editing an existing location
            hudView.text = "Updated"
            location = temp
        } else {
            hudView.text = "Tagged"
            
            //create the Location core data instance.
            location = Location(context: managedObjectContext)
            location.photoID = nil
        }
        
        //After location instance has been created if we're not editing, we can use the location variable like any other object
        location.locationDescription = descriptionTextView.text
        location.category = categoryName
        location.longitude = coordinate.longitude
        location.latitude = coordinate.latitude
        location.date = date
        location.placemark = placemark
        
        //save image
        if let image = image {
                if !location.hasPhoto {
                location.photoID = Location.nextPhotoID() as NSNumber
            }
            if let data = image.jpegData(compressionQuality: 0.5) {
                do {
                    try data.write(to: location.photoURL, options: .atomic)
                } catch {
                    print("Error writing file: \(error)")
                }
            }
        }
        
        do {
            try managedObjectContext.save()
            //Calls free function afterDelay that accepts a parameters and a closure with no arguments
            afterDelay(0.6) {
                hudView.hide()
                self.navigationController?.popViewController(animated: true)
            }
        } catch {
            fatalCoreDataError(error)
        }
    }
    
    @IBAction func cancel() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func hideKeyboard(_ gestureRecognizer: UIGestureRecognizer) {
        let point = gestureRecognizer.location(in: tableView) //returns a point where the tap happened
        let indexPath = tableView.indexPathForRow(at: point)
        
        if indexPath != nil && indexPath!.section == 0 && indexPath!.row == 0 {
            return
        }
        descriptionTextView.resignFirstResponder()
    }
    
    @IBAction func categoryPickerDidPickCategory(_ segue: UIStoryboardSegue) {
        let controller = segue.source as! CategoryPickerViewController
        categoryName = controller.selectedCategoryName
        categoryLabel.text = categoryName
    }
    
    //Mark:- Table View Delegates
    
    /* willSelectRow allows pre-defined behaviors to occur before tapping on a cell. In this case, the behavior is to limit taps
       to just the cells from the first two sections */
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        //might be able to remove indexPath.section
        if indexPath.section == 0 || indexPath.section == 1 {
            return indexPath
        } else {
            return nil
        }
    }
    
    //Handles the actual taps on the rows
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //We do not need to create behavior for handling tapping for category or photo as these are connected as segues in the storyboard
        if indexPath.section == 0 && indexPath.row == 0  {
            descriptionTextView.becomeFirstResponder()
        } else if indexPath.section == 1 && indexPath.row == 0 {
            tableView.deselectRow(at: indexPath, animated: true) //prevents the Add Photo Cell from staying grey after the alert action pops up
           pickPhoto()
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let selection = UIView(frame: CGRect.zero)
        selection.backgroundColor = UIColor(white: 1.0, alpha: 0.3)
        cell.selectedBackgroundView = selection
    }

    
    //Mark:- Help Methods
    func string(from placemark: CLPlacemark) -> String {
        //new lines aren't necessary here because the lables will wrap.
        var line = ""
        line.add(text: placemark.subThoroughfare) //house number
        line.add(text: placemark.thoroughfare, separatedBy: " ") // street name
        line.add(text: placemark.locality, separatedBy: ", ") //city
        line.add(text: placemark.administrativeArea, separatedBy: ", ") // province
        line.add(text: placemark.postalCode, separatedBy: " ") //postal code
        line.add(text: placemark.country, separatedBy: ", " )
        
        return line
    }
    
    //Asks the DateFormatter to turn the Date into a String and returns that
    func format(date: Date) -> String {
        return dateFormatter.string(from: date)
    }
    
    func listenForBackgroundNotification() {
        observer = NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification, object: nil, queue: OperationQueue.main) { [weak self] _ in
            //UIViewController's presentedViewController property has a reference to the modal (our pop up if displayed) view controller.
            if let weakSelf = self {
                if weakSelf.presentedViewController != nil {
                    weakSelf.dismiss(animated: false, completion: nil)
                }
                weakSelf.descriptionTextView.resignFirstResponder()
            }
        }
    }
}


//Extension to use camera and photo library
extension LocationDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK:- Image Picker Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        //We use optional cast because image can indeed be nil if user doesn't select one.
        image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Image Helper Methods
    func pickPhoto() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            showPhotoMenu()
        } else {
            choosePhotoFromLibrary()
        }
    }
    
    func showPhotoMenu() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let chooseCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(chooseCancel)
        
        let choosePhoto = UIAlertAction(title: "Take Photo", style: .default, handler: { _ in
            self.takePhotoWithCamera()
        })
        alert.addAction(choosePhoto)
        
        let chooseLibrary = UIAlertAction(title: "Choose From Library", style: .default, handler: {_ in
            self.choosePhotoFromLibrary()
        })
        alert.addAction(chooseLibrary)
        
        present(alert, animated: true, completion: nil)
    }
    
    func takePhotoWithCamera() {
        let imagePicker = MyImagePickerController()
        imagePicker.sourceType = .camera 
        imagePicker.delegate = self
        imagePicker.allowsEditing = true //with this enabled the user can do some quick editing (cropping) of the photo
        imagePicker.view.tintColor = view.tintColor
        present(imagePicker, animated: true, completion: nil)
    }
    
    func choosePhotoFromLibrary() {
        let imagePicker = MyImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.view.tintColor = view.tintColor
        present(imagePicker, animated: true, completion: nil)
    }
}
