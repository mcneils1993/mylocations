//
//  AppDelegate.swift
//  MyLocations
//
//  Created by Simon McNeil on 2019-06-16.
//  Copyright © 2019 SimonMcNeil. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    //Code the create our NSManagedObjectModel
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "DataModel")
        
        //loads the data from the database into memory and sets up the Core Data Stack
        container.loadPersistentStores(completionHandler: { storeDescription, error in
            if let error = error {
                print("Could load data store: \(error)")
            }
        })
        return container
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = persistentContainer.viewContext

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        customizeAppearance()
        
        //Pass are managedObjectContext variable to our managedObjectContext var that is in the CurrentLocationViewController
        let tabController = window!.rootViewController as! UITabBarController
        if let tabViewControllers = tabController.viewControllers {
            //First Tab
            var navController = tabViewControllers[0] as! UINavigationController
            let controller1 = navController.viewControllers.first as! CurrentLocationViewController
            controller1.managedObjectContext = managedObjectContext
            
            //Second tab
            navController = tabViewControllers[1] as! UINavigationController
            let controller2 = navController.viewControllers.first as! LocationsViewController
            controller2.managedObjectContext = managedObjectContext
            let _ = controller2.view //adding this as a crash can happen for older iPhone versions. After iOS 12 is fine.
            
            navController = tabViewControllers[2] as! UINavigationController
            let controller3 = navController.viewControllers.first as! MapViewController
            controller3.managedObjectContext = managedObjectContext
            
        }
        listenForFatallCoreDataNotifications()
        print(applicationsDocumentsDirectory)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the backgro und state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func customizeAppearance() {
        //navigation bar
        UINavigationBar.appearance().barTintColor = UIColor.black
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        
        //tab bar
        UITabBar.appearance().barTintColor = UIColor.black
        UITabBar.appearance().tintColor = UIColor(displayP3Red: 255/255.0, green: 238/255.0, blue: 136/255.0, alpha: 1.0) //color of the tab options text
    }


    //MARK:- Helper Methods
    func listenForFatallCoreDataNotifications() {
        //1. Tell notification center that you want to be notified whenever a CoreDataSaveFailedNotification is posted.
        NotificationCenter.default.addObserver(forName: CoreDataSaveFailedNotification, object: nil, queue: OperationQueue.main, using: { notification in
            
            //Setup Error Message to be displayed
            let message = """
                There was a fatal error in the app and it cannot continue.

                Press OK to terminate the app. Sorry for the inconvenience
            """
            
            //Create UIAlertController
            let alert = UIAlertController(title: "Internal Error", message: message, preferredStyle: .alert)
            
            
            let action = UIAlertAction(title: "OK", style: .default, handler:  { _ in
                //instead of calling fatalError(), the closure creates an NSException object to terminate the app.
                let exception = NSException(name: NSExceptionName.internalInconsistencyException, reason: "Fata Core Data Error", userInfo: nil)
                exception.raise()
            })
            alert.addAction(action)
            
            /* To show the alert, we need a view controller that is currently visible. Simply use the windows root view controller
               Which a tabBarViewController in this app, and it will be visible at all times as per the current navigation flow of the app.
             */
            let tabController = self.window!.rootViewController!
            tabController.present(alert, animated: true, completion: nil)
        })
    }
}

