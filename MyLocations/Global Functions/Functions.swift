//
//  Functions.swift
//  MyLocations
//
//  Created by Simon McNeil on 2019-07-05.
//  Copyright © 2019 SimonMcNeil. All rights reserved.
//


//This Class is used to create free functions, meaning they can be used anywhere
import Foundation
import UIKit

let CoreDataSaveFailedNotification = Notification.Name(rawValue: "coreDataSaveFailedNotification")

var hasSafeArea: Bool {
    guard #available(iOS 11.0, *), let topPadding = UIApplication.shared.keyWindow?.safeAreaInsets.top, topPadding > 24 else {
        return false
    }
    print(topPadding)
    return true
}


let applicationsDocumentsDirectory: URL = {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths [0]
}()

func afterDelay(_ seconds: Double, run: @escaping () -> Void) {
    //The annotation @escaping, is necessary for closures that are not performed immediately. Therefore is needed to run GCD function asyncAfter
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: run)
}

//global function for handling fata core data errors
func fatalCoreDataError(_ error: Error) {
    print("*** Fatal Error: \(error)")
    NotificationCenter.default.post(name: CoreDataSaveFailedNotification, object: nil)
}


