//
//  HudView.swift
//  MyLocations
//
//  Created by Simon McNeil on 2019-07-05.
//  Copyright © 2019 SimonMcNeil. All rights reserved.
//

import UIKit

class HudView: UIView {
    var text = ""
    
    /* This is a convenience constructor, known as a class method. It is a method that works on the class as whole and not a particular instance
     
       The whole purpose of doing it this way is so that we create an instance of the view for ourselves, so we don't have to do it, and can place it
       on top of another view.
     */
    class func hud(inView view: UIView, animated: Bool) -> HudView {
        let hudView = HudView(frame: view.bounds) // HudView(frame:) is an init method inherited from UIView. This is where we create the making of the instance
        hudView.isOpaque = false
        
        view.addSubview(hudView)
        view.isUserInteractionEnabled = false
        hudView.show(animated: animated)
        return hudView //New instance is returned to the caller
    }
    
    //MARK:- Public Methods
    func show(animated: Bool) {
        if animated {
            alpha = 0 //set initial state of the view
            transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: [], animations:  {
                self.alpha = 1 //sets up state of the view for how it should be when completes
                self.transform = CGAffineTransform.identity //identity returns its scale back to normal
            }, completion: nil)
        }
    }
    
    func hide() {
        superview?.isUserInteractionEnabled = true
        removeFromSuperview()
    }
    
    //Draw method is invoked whenever UIKit wants your view to redraw itself
    override func draw(_ rect: CGRect) {
        let boxWidth: CGFloat = 96
        let boxHeight: CGFloat = 96
        
        //center the HUD, to change the size simply modify the boxWidth and boxHeight values
        let boxRect = CGRect(x: round((bounds.size.width - boxWidth) / 2), y: round((bounds.size.height - boxHeight) / 2), width: boxWidth, height: boxHeight)
        let roundedRect = UIBezierPath(roundedRect: boxRect, cornerRadius: 10)
        
        UIColor(white: 0.3, alpha: 0.8).setFill()
        roundedRect.fill()
        
        /* Loads the checkmark into a UIImage object,
           then calculates the position for that image
           based on the center of the coordinates of the HUD View, and dimensions of the image */
        if let image = UIImage(named: "Checkmark") {
            let imagePoint = CGPoint(
                x: center.x - round(image.size.width / 2),
                y: center.y - round(image.size.height / 2) - boxHeight / 8)
            image.draw(at: imagePoint)
        }
        
        //Draw text
        let attribs = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        
        let textSize = text.size(withAttributes: attribs)
        let textPoint = CGPoint(
                x: center.x - round(textSize.width / 2),
                y: center.y - round(textSize.height / 2) + boxHeight / 4)
        
        text.draw(at: textPoint, withAttributes: attribs)
    }
}

