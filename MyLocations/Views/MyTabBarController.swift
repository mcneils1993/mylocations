//
//  MyTabBarController.swift
//  MyLocations
//
//  Created by Simon McNeil on 2019-08-07.
//  Copyright © 2019 SimonMcNeil. All rights reserved.
//

import UIKit

class MyTabBarController: UITabBarController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //By returning the childForStatusbarStyle, the tab bar controller will look at its own preferredStatusBarStyle property instead of those from other view controllers
    override var childForStatusBarStyle: UIViewController? {
        return nil
    }
}
