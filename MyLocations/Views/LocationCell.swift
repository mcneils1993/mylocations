//
//  LocationCell.swift
//  MyLocations
//
//  Created by Simon McNeil on 2019-07-15.
//  Copyright © 2019 SimonMcNeil. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //custom background for when selecting a cell
        let selection = UIView(frame: CGRect.zero)
        selection.backgroundColor = UIColor(white: 1.0, alpha: 0.3)
        selectedBackgroundView = selection
        
        //Round corners for images
        photoImageView.layer.cornerRadius = photoImageView.bounds.size.width / 2
        photoImageView.clipsToBounds = true
        separatorInset = UIEdgeInsets(top: 0, left: 82, bottom: 0, right: 0) //shifts the separator to the left until right after the image
        
        //used to check how long our labels are for constraint purposes
//        descriptionLabel.backgroundColor = UIColor.purple
//        addressLabel.backgroundColor = UIColor.purple
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Helper Method
    func configure(for location: Location) {
        
        if location.locationDescription.isEmpty {
            descriptionLabel.text = "No Description"
        } else {
            descriptionLabel.text = location.locationDescription
        }
        
        //get the Location object for the row from the array and then use its properties to fill the labels. Since placemark is optional, we use if let
        if let placemark = location.placemark {
            var text = ""
            text.add(text: placemark.subThoroughfare) //house number
            text.add(text: placemark.thoroughfare, separatedBy: " ") //street name
            text.add(text: placemark.locality, separatedBy: " ") //city
            addressLabel.text = text
        } else {
            addressLabel.text = String(format: "Lat: %.8f, Long: %.8f", location.latitude, location.longitude)
        }
        photoImageView.image = thumbnail(for: location)
    }
    
    func thumbnail(for location: Location) -> UIImage {
        //think of this as if location has photo and I can unwrap location.photoImage. Then return the photo.
        if location.hasPhoto, let image = location.photoImage {
            return image.resized(withBounds: CGSize(width: 52, height: 52))
        }
        
        //Recall that UIImaged(named:) is a failable initializer, so it returns an optional
        return UIImage(named: "No Photo")!
    }
}
