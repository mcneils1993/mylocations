//
//  MyImagePickerController.swift
//  MyLocations
//
//  Created by Simon McNeil on 2019-08-07.
//  Copyright © 2019 SimonMcNeil. All rights reserved.
//

import UIKit

/* Class to override the status bar style */
class MyImagePickerController: UIImagePickerController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
