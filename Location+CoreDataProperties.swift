//
//  Location+CoreDataProperties.swift
//  MyLocations
//
//  Created by Simon McNeil on 2019-07-08.
//  Copyright © 2019 SimonMcNeil. All rights reserved.
//
//

import Foundation
import CoreData
import CoreLocation

/* The extension allows us to add additional functionality to an existing object without having to change its original source code for that object */
extension Location {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Location> {
        return NSFetchRequest<Location>(entityName: "Location")
    }

    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var date: Date
    @NSManaged public var locationDescription: String
    @NSManaged public var category: String
    @NSManaged public var placemark: CLPlacemark?
    @NSManaged public var photoID: NSNumber?

}
